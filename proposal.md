#Additional Features and Portability to mEGADATA
-Vistaar Juneja
##Project Information
###Project Title/Abstract: 
Implementing additional features to the mEGADATA application to ease submission of metadata to the EGA, and making the application portable for use by other genomics projects in different environments.
###Project short Title: 
Additional Features and Portability to mEGADATA
###URL of project idea page: 
https://bitbucket.org/mugqic/gsoc2016/overview#markdown-header-ega-data-submission-database-megadata

##Biographical Information
I'm a sophomore majoring in Computer Science and Engineering at the Indian Institute of Technology (IIT) Guwahati, which is one of the best technical institutes of India. I have a keen interest in Mathematics and Computer Science, and have worked on many projects during my school and college years. I’ve completed three semesters of course-work which include several lab and theory courses relevant to this project like Data Structures, Algorithms, Software Engineering Lab, Systems Programming and Modern Biology.
I am dedicated and hardworking, and have a strong academic background with good problem solving and programming skills. I’ve excelled in various national competitions, the details of which can be seen in my resume(https://drive.google.com/file/d/0B_FwqVpLc3fMczdfSmRDbl8wN2M/view?usp=sharing). I’ve also completed many projects using Python, PHP, Javascript, etc and have used APIs in multiple projects, which can be viewed from my github profile(https://github.com/VistaarJ).
I thoroughly enjoy learning new technologies and was part of the runner-up team in the Microsoft Code.Fun.Do hackathon where I learnt MS-Word’s JS API from extremely limited resources and implemented a winning working prototype in less than 24 hours. I realise how important this project can be to the scientific community and would like to work with the mentor to bring this project to completion. 

##Contact Information 
###Name: 
Vistaar Juneja
###Melange link ID: 
vistaarjuneja@gmail.com 
###Email:
vistaarjuneja@gmail.com, vistaar@iitg.ernet.in
###Github:
VistaarJ
###Telephone:
+(91) 7896880505, 9999263812
###Student postal address:
Room No. B-243, Umiam Hostel, IIT Guwahati, Assam 781039
###Resume:
https://drive.google.com/file/d/0B_FwqVpLc3fMczdfSmRDbl8wN2M/view?usp=sharing

##Student Affiliation
###Institute - 
Indian Institute of Technology (IIT) Guwahati, India
B.Tech in Computer Science and Engineering
###Stage of completion:
Sophomore (enrolled from 2014-2018)
###Contact to verify: 
hosadmin@iitg.ernet.in

##Schedule Conflicts 
I have no other commitments for the summer and the Canadian Centre for Computational Genomics is the only organisation which I've applied to in GSOC. Thus, I will be able to work on the project with full focus and dedication for 50+ hours in a week.

##Mentors
###Mentor names: 
David Bujold
###Mentor emails: 
david.bujold@computationalgenomics.ca
###Mentor link_ids: 
https://bitbucket.org/dbujold/
###Have you been in touch with the mentors? 
Yes, I've been in touch with the mentor via emails constantly through the course of the application period.

##Synopsis              
The European Genome Archive (EGA) is one of the most popular controlled access repositories for accessing information on a variety of sequence and genotype experiments. It serves as a platform where a single organisation's work can potentially be used and analysed by thousands of organisations, with the appropriate permissions of the relevant Data Access Committee (DAC). It also involves the transfer of an extremely large amount of data on a daily basis. In such a situation, it becomes extremely important that an organisation have the right tools at its disposal to upload and download such large amounts of data in the most efficient way possible. The mEGADATA application is a database-driven lightweight web application which automates the generation of proper XMLs while submitting metadata to the EGA. In this project, the goal would be implement additional features and improve on existing components like assay and analysis metadata management, methods to update/delete metadata components, a global search application, porting existing perl scripts to python, introducing application authentication etc. The final and most important goal would be to make the web application available as a Docker container, using which other scientists working in different environments would be able to derive benefits of the application without any hassle.

##Benefits to Community
mEGADATA is a tool which is desperately needed by scientists to ease metadata submission for large uploads containing hundreds of samples. Currently, the best tool available is the XMLS service available at https://www-test.ebi.ac.uk/ena/submit/drop-box/submit/ as a SRA REST submission form, and filling out such forms for hundreds of samples is an extremely time-consuming task. The mEGADATA web application will provide an easy to use UI (using the Handsontable library) which gives an Excel-like interface to log all the metadata, and then generate the XMLs in the proper format. Making this application easily portable and usable without having to address complicated setup will be of great benefit to many scientists and organisations. Hopefully, the application becomes so easy to use that it inspires other scientists to submit their work to the EGA and increase the interaction and bonding between the genomics community. A further add on will be to provide scientists with statistics regarding their data, making analysis and derivations much easier. The main focus of any scientist should be on doing research and analysis, rather than figuring out how to upload/share data. Hopefully, mEGADATA will ensure that data submission becomes extremely easy.
Working on this project would be extremely beneficial to me as well, because I would get to see my work being used by the scientific community - which is a really satisfying feeling. I would also get to learn more about new libraries and technologies, and experience first-hand the development of a software to completion. 

##Coding Plan and Methods

###Assays metadata Management 
Completing the assay metadata component would involve the following steps:

(1) Setting up a static page for the experiment metadata, which would include a page to enter the information in an editor-style mode and a page to view all the content using the Handsontable library. This would be pretty similar to the donor and sample metadata already implemented. The HTML page will ask for the information and the js page will include the Handsontable Renderers, along with the Methods and Constructors required to get the Excel-like display that we see.

(2) Currently, the experiments are stored as collections and then assigned to different datasets. The JSON dump for the experiments is available at /api/experiments. In order to add experiment metadata, the following MySQL accession functions need to be implemented:


    insertExperiment()
    insertExperimentMetadata()

These would have API accession points at /api/experiment and /api/experiment_metadata, and appropriate changes would be made to the  API routes.

    @app.route("/api/experiment", methods=['POST'])
    def route_api_experiment_add():
        return Response(insertExperiment(), mimetype='application/json')


    @app.route("/api/experiment_metadata", methods=['POST'])
    def route_api_experiment_metadata_add():
        return Response(insertExperimentMetadata(), mimetype='application/json')

(3) Creating the appropriate models (in this case, it would be ExperimentMetadata) and ensuring that foreign keys are declared in a manner which disallows any action violating the integrity of the databases. A user should not be allowed to delete metadata sets used by other sets, as this may break the structure of the database.

The analysis metadata component can be constructed in a similar fashion to the above. 

###Modifying Metadata control operations
Currently, only creation of sets is supported by the user interface, and other modifications have to be done via the database. We need to ensure that the user can modify, delete and replace datasets easily on the interface itself.
For this, we would need to set up API endpoints using the PUT, PATCH and DELETE HTTP methods.

(1) PUT performs update/replace operations.

(2) PATCH performs update/modify operations.

(3) DELETE deletes a particular collection.

This would need to be done for all the metadata components. As an example, consider the Sample metadata:

a delete MySQL accession function would have to be made which would be accessed using

    @app.route("/api/samples/<donor>", methods=['DELETE'])
    def route_api_delete_sample(donor=None):
        return Response(deleteSampleList(donor), mimetype='application/json')

The accession function would simply loop over the Sample database and delete all the samples with the specified donors as foreign keys. The PATCH and PUT endpoints can be accessed similarly.
After completing this, all that is left is to edit the Handsontable javascript file to include methods which access the specific endpoints and perform the required operation.
I believe the challenge here would be to ensure that all the API access points work and that information can be successfully edited in the database. This is because PATCH and DELETE commands work on only a limited number of syntaxes. This can be avoided by using the flask documentation for PATCH and DELETE integration as a reference point and not deviating too much from the established syntax.

###Global Search Application 
Any information available in the web application will definitely be procured from the database itself, so we would be needing a fast and efficient way to search either the SQL database or the JSON objects for any type of value.
Any script which searches the database like the one available at http://vyaskn.tripod.com/search_all_columns_in_all_tables.htm is extremely slow and takes multiple minutes even for a really small database.
The best technique to accomplish this would be to loop over the API endpoints, store the JSON in JSON objects and then search for the given string in them. This can easily be accomplished by a Python script.

    routes=["/api/donors","/api/experiments","/api/samples"] #loops over the api routes (only 3 included here for convenience)
    url="http://localhost:5000" #base url
    for ext in routes: #loops over the set
        page=url+ext #url of the api access point
        response=urllib.urlopen(str(page)) #connects to the page
        data=json.loads(response.read()) #data is a python dictionary containing dump of the endpoint
        #Now, we can simply search for the string to be searched in data, and output it in a neat format if it is there

The biggest challenge with the search application is tracing back the JSON object in case a string is found and outputting the information in pretty manner. This can be solved by either using custom made formats for each type of data or using a more generic based script which follows the same procedure for each type of data (this may run into problems for some special cases).

###Porting Perl Scripts to Python 
Currently, the script to create the EGA submission XMLs from what is in the database is written in Perl. It would be better to port this script to Python so it can maintain a continuity with the rest of the project and also be integrated with the rest of the application interface.
This is a very easy task in Python. You have to first install an adapter depending on the database that you are using (psycopg2 for PostGreSQL and MySQLdb for MySQL). I have tried both of them and they work perfectly. The following script reads data from a MySQL database and outputs it as XML into a file:

    #!/usr/bin/python
    import MySQLdb

    outfile=file(outfileName,'w') #write output to outfile
    db = MySQLdb.connect(host="localhost",    
                         user="username",         # username
                         passwd="password",          # password
                         db="nameofdatabase")        # name of the data base

    cur = db.cursor() #this is a Cursor object which lets you execute all the needed queries

    cur.execute("SELECT * FROM TABLE_NAME") #creates an object cur holding all the contents of TABLE_NAME
    outfile.write('<?xml version="1.0" ?>\n') #write XML data to the file
    outfile.write('<mydata>\n')
    for row in cur.fetchall():
        #Print out the parameters in XML format
        outfile.write('    <first>%s</first>\n' % row[0])
        outfile.write('    <second>%s</second>\n' % row[1])
        #and so on
    outfile.write('</mydata>\n')
    outfile.close()
    db.close()

The above Python script automates the generation of XML files from database files, thus replacing the Perl file and allowing it to be integrated with the rest of the application interface. Note that some modifications like ensuring proper indentation would have to be automated in order for the script to work.
The challenge here is ensuring that the output scripts are in the exact format as those required for submission to the EGA. Any deviation from the proposed model might result in an invalid upload. Thus, we would need to check that the script works for all types of special characters and XML metadata submissions. A contingency method which shows the appropriate message in case of broken XML is of prime importance here.

###Bash Scripts to Automate Raw data transfer
With the mEGADATA application, the transfer of raw data will also become significantly easier. Since mEGADATA already contains the paths to the raw files on the target server, it is just a matter of generating bash scripts which automatically use Aspera to upload the data.
The above can also be achieved using a simple FTP client, however, Aspera offers speeds which are up to 100 times faster than normal FTP clients.
Essentially, the bash script will contain the following commands:

(1) Since the paths of all the raw data files are available in the mEGADATA application, the bash script would simply loop over them (a good way to do this would be to dump the contents of the database and then simply extract the contents of the required table).

(2) For each file path, it would use the Aspera ascp command line program to complete the upload, as demonstrated at https://www.ebi.ac.uk/ega/submission/tools/FTP_aspera.
    ascp -QT -l300M -L- <file to upload> <account_log_in>@fasp.ega.ebi.ac.uk:/.
The upload speed could be added by the user while running the bash script (in the above example it is 30MB/s)
<file to upload> would be the current file which is to be uploaded (accomplished by looping through all paths)
The user would just have to supply his login credentials and the bash script would automate this data transfer for him.
The python script should be able to handle special characters in file paths. This can be done easily by escaping characters.

###Database Management for Sequencing Runs
The sequencing runs are connected to the run table in the database. Currently, there's almost no metadata being kept for runs in the database - so that needs to be added, along with a easy to use UI. This database will store information like the paths to the raw data files along with md5 checksums for integrity checks.The run XML is used to associate data files with experiments and typically comprises of a single data file. The format for the file is available at https://www.ebi.ac.uk/ega/sites/ebi.ac.uk.ega/files/documents/Run.xml.

(1) Raw data server location needs to be uploaded to the database. It could be uploaded in two formats - either as paths to multiple folders or paths to multiple files. A script would scan the folders and get the paths to all the individual raw data files available on the server location.

(2) We would again use the Handsontable library to get the user to enter information. It would be a simple path uploading system which can upload both folder and file paths - this can easily be implemented using AngularJS. We would then individually scan the folders to get all the raw data file paths.

(3) The next step is to store md5 checksums for performing integrity checks. This can be very useful while comparing files and ensuring the integrity of data sent to the EGA. In this case, since the actual raw data files will be extremely large, we would have to read them in chunks of 4096 bytes. This can be done using a simple python script:

    def md5(fname): #fname is the name of the file to be hashed
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""): # reads in the file 4096 bytes at a time
                hash_md5.update(chunk) #updates the checksum
        return hash_md5.hexdigest() #The digest is the concatenation of strings fed to the update function

###Bioinformatics downstream analysis metadata
The sequencing files contain pieces of DNA sequence, but don't reveal any other information regarding them. Bioinformatic downstream analysis is required to extract meaningful data out of these sequencing files.
Downstream analysis includes:

(1) Filtering for bad/unrecognized data

(2) Generation of statistics so that the user can recognize any potential bugs or do further analysis of the data.

(3) Figuring out where pieces of DNA belong on a reference genome

(4) Using software which can read the extremely large FASTQ files in a fast and efficient way

(5) Displaying information about variants - such as their positions on a reference sequence, the reference and alternate alleles etc.

(6) Aligning or mapping the data, which is an essential step in re-sequencing.

Now, there would be a lot of additional processing to be done on this data. The softwares which accomplish these tasks would need to be entered into the database along with relevant information (like their versions, parameters, licenses etc). This can be helpful in the future when a software is upgraded, and gives information on how the analysis of a particular dataset is performed. The metadata would be entered in a similar way as the donor and sample metadata is entered (creating an output page for entering/editing the information, updating the models, making accession functions, and finally setting up api routes and access points). A drop-down menu with a list of all softwares could be implemented here.

###Application Authentication 

Flask-Stormpath makes it extremely easy to register, login, and logout users. The best thing is that it provides built-in support for integrating services like Facebook and Google login to allow for instant access.
To enable Google login support for the web application, we would need to:

(1) Log into the Google Developer Console and create a new Google project - pick a Project Name and ID.

(2) Enable Google login by toggling the "Google+API" status to on

(3) Create OAuth credentials and configure the settings accordingly (the application type, public and local URL of the site have to be specified). After this, Client ID and Client Secret variables will be revealed, which are needed while configuring the mEGADATA application.

(4) Now, all we have to do is integrate Google OAuth support into the web-app using the credentials:

    from os import environ

    app.config['STORMPATH_ENABLE_GOOGLE'] = True
    app.config['STORMPATH_SOCIAL'] = {
        'GOOGLE': {
            'client_id': environ.get('GOOGLE_CLIENT_ID'),
            'client_secret': environ.get('GOOGLE_CLIENT_SECRET'),
        }
    }

This finishes up OAuth support and allows users to login using simply their Google accounts.
Some additional functionality which can be added:

(1) Enabling facebook login support for those who do not have Google accounts.

(2) Enabling caching: Flask-Stormpath has built in support for caching. Proper use of caching results in much faster websites.

###Application Portability

This is a very important aspect of the entire project - making the application easily portable so that other scientists can use it without having to address complicated setup. Scientists should be able to successfully use the application irrespective of their computer setups.
I have read around a lot and found that using Docker would be the best fit for this project for the following reasons:

(1) Docker gives you the ability to snapshot the OS into a common image and makes it easier to deploy on other docker hosts. This is especially great for applications requiring unit testing with connections to a database to be run serially. With Docker, you can simply create an image of the database and run all the tests in parallel as they will be running against the same snapshot. However, doing this on a VM would be much more time consuming and all the tests would be required to run disjointly in series, so that they don't step on each other. Since accessing information from databases is a frequently performed operation in this project, there would be a significant increase in efficiency if we use Docker containers instead of regular VMs.

(2) It is extremely lightweight and manages the networking for you as well.

(3) In this particular project, we would want to isolate processes from each other and run them on a host - and Docker containers make this extremely fast (a matter of seconds).

(4) The goal of Docker is to help developers port applications with all of the dependencies conjointly - without any headache.

Docker achieves this by creating safe LXC (Linux Containers) based environments for applications called docker containers. These containers are created using docker images, which can be built by executing commands through Dockerfiles.

(1) Docker can be installed using the instructions available at https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-python-web-applications.

(2) We would have to create a dockerfile which automatically builds the image for us. Dockerfiles are essentially scripts which contain successively declared commands - which are to be executed in that order by the docker to create a new image. It would contain commands like:

    Setting base image to Ubuntu
    Adding author/maintainer name
    Adding the application resources URL and updating the sources list
    Installing basic applications and tools
    Copying the application folder inside the container
    Installing the requirements needed for the project (in requirements.txt)
    Exposing ports and setting default directory where execution will take place
    Setting the default command to execute when creating a new container

(3) Once we have successfully built the Dockerfile, then building containers becomes an extremely easy and automatic process. Now, we can build a new container image and finally run the application. This completes the process to run the application via a docker container.

Also, migrating the mySQL database to an SQLite database will be quite easy as peewee gives quite a generic object-relational mapping, which is easy to port. We would just have to change the relevant queries/methods of insertion into the database slightly. 

The main challenges associated with Docker is ensuring that the Dockerfiles  have sufficient information so that they can be deployed on any OS with any configurations. The container images generated by the dockerfiles need to be subjected to heavy testing (like unit testing, integration testing) to ensure that the web-app is easily deployable in all types of environments.

##Proposal Timeline
###Pre-GSOC
    
(1) Try to solve some issues with the existing code and submit pull requests for the same.

(2) Submit any issues that I encounter so that the contributors can have a look at them and provide comments.

(3) Familiarize myself with the work of the EGA and how to complete a proper submission, along with the shortcomings that the current method poses (I've been unable to look at a study/dataset yet despite sending a mail to the relevant DAC that it was only for educational purposes).

###Apr 22 - May 23 (Community Bonding Period) :

(1) Read through the existing codebase and understand all the libraries and functions that have been used.

(2) Solve some issues with the existing code and submit pull requests for the same.

(3) Work with the mentor to understand some of the potential challenges/problems that might occur while finishing the application to completion.

(4) Work with the mentor so that existing codebase is completely bug-free and works perfectly.

###May 23 - Aug 23 (Official Work Period)

####Week 1 (May 23 - May 30)
(1)Complete the experiment metadata component which is currently in the database - this would involve setting up API access points and finishing up the accession functions.

(2) Complete the analysis metadata component and provide an interface through which the components can be added.

####Week 2 (May 30 - Jun 6)
(1) Work on the sequencing run database which would include file paths, md5 checksums, etc.

(2) Provide a user interface (using AngularJS and the Handsontable library) through which all such data can be entered into the database. Generate a script using which a folder can be scanned to get all file paths for the raw data.

(3) Testing: Ensure that md5 checksums are properly generated for large files, and develop a mechanism to check for integrity/correctness of files.

####Week 3 (Jun 6 - Jun 13)
(1) Create a table for maintaining bioinformatics downstream analysis metadata - which will maintain relevant information for the softwares used.

(2) Provide an easy-to-use interface using which all such data can be entered into the database (preferably, a drop-down menu containing list of all possible softwares, with their versions, parameters etc).

####Week 4 (Jun 13 - Jun 20)
(1) Create a toolbar which can perform a search on the entire application

(2) Optimise the script to give results as fast as possible.

(3) Testing: Make sure that search works for all types of data, which may include special characters - eg, the user should be able to get information related to whatever string he/she searches. Also ensure that once the string is found, the JSON object can be traced back to get all the other characteristics of the data correctly and efficiently.

####Week 5 (Jun 20 - Jun 27)
(1) Create a bash script which can automate the uploading of raw data files to Aspera using the file paths available in the database.

(2) Testing: Make sure that the bash script gives correct response in case of invalid/corrupted file paths, and ensure that it works for paths containing all types of special characters.

###June 27 (Midterm Evaluation Deadline)

####Week 6 (Jun 27 - Jul 4)
(1) Port the perl scripts for creating the EGA submission XMLs to Python.

(2) Develop a script/method which gives appropriate messages in case of broken XML syntax. This is to ensure that the user can do manual corrections in case some error occurs.

(3) Testing: The XMLs should be in the exact format as required by the EGA for metadata submission. Ensure that the script can handle all types of special cases like updating XML versions, escaping special characters etc.

####Week 7 (Jul 4 - Jul 11)
(1) Create a custom Handsontable editor which can map metadata properties to an ontology using bioportal.bioontology.org.

(2) Clean up all the implemented code (adding comments, doing bug fixes, etc) for the final stage of the project - setting up authentication and portability

####Week 8 (Jul 11 - Jul 18)
(1) Implement application authentication using Google OAuth2 so that users can log in without requiring registration.

(2) Add other features like implementing Facebook Login support and enabling caching for speeding up the authentication process.

####Week 9  (Jul 18 - Jul 25)
(1) Create methods to modify/delete information for all the implemented metadata through an easy-to-use interface.

(2) Implement other features which might be of relevance - like using an API to give information about a particular cell type or tissue type.

(3) Testing: Ensure that all the metadata components work correctly by doing unit testing. Make sure that PUT and PATCH requests give correct responses and have fool-proof mechanisms in case of failures or unrecognised parameters.

####Week 10 (Jul 25 - Aug 1) and Week 11 (Aug 1 - Aug 8)
(1) Port all the MySQL to SQLite.

(2) Create the dockerfile which can automatically build images 

(3) Do heavy testing inside the container and run integration tests, unit tests for all the individual components of the application. Ensure that the docker containers are functional and are working for all types of computer configurations like different operating systems, etc.

####Week 12 (Aug 8 - Aug 15)
Buffer Period  

####Week 13 (Aug 15 - Aug 23)
Final testing, bug fixes and documentation.

Note that testing, fixing bugs, and documentation will be a continuous process, and the final week is completely devoted to the same.
As I have thoroughly planned out each and every aspect of the project and have already researched how they will be implemented, the probability of things going wrong is quite low. However, a contingency plan must be ready in case things do go wrong. In such a situation, my plan is to work with the mentor and discuss possible reasons why things may not be working as desired, and how I can correct them. The plan in such a situation would be to complete the current week’s work within the next week at maximum. I have specially designed the timeline so as to have alternate weeks in terms of time/commitment required to the project. This makes it easier to accommodate problems when things do not go according to schedule.
August 23 (Final Evaluation Deadline)

##Management of the Project 
My method of submitting/contributing to the codebase will be to generate a pull request from a branch of my forked repository at https://bitbucket.org/genap/megadata. Before generating a pull request, I would first do exhaustive unit testing of the component that has been changed/modified, and ensure that it does not  cause any breaks/dependency failures in the rest of the code. The next step would be to generate a pull request with the testing information and ask for the mentor's opinion on how to proceed. If there are some leftover changes to be done before merging, I would complete them and submit a pull request once again.
I plan to commit at least once everyday during the coding phase – the commit could involve fixing a bug in the existing codebase or working on a new feature.
In my opinion, any sort of erratic commit behavior where all the commits are done just prior to a deadline would be problematic. The best way of completing any software project is to do continuous development and testing over the given period of time, so I would definitely follow that approach.

##Test
(1) I submitted the selection test required to be done for the project at https://github.com/VistaarJ/REST-API-Using-Flask-.

(2) I've made a pull to the project which has been merged. Details can be viewed at https://bitbucket.org/genap/megadata/pull-requests/?displaystatus=merged.

(3) I've also filed an issue at https://bitbucket.org/genap/megadata/issues?status=new&status=open which has been solved by the mentor.

(4) I've worked on many projects in Python, PHP, MySQL and Javascript which can be seen on my github account(https://github.com/VistaarJ/).

The selection test I developed is a very simple implementation of a REST application using Flask. It uses a Flask extension known as APIManager to automate HTTP requests sent to the server. APIManager already has inbuilt mechanisms to manage failures and is a very fast and simple way of creating a REST application. My implementation supports GET, PUT, POST, PATCH and DELETE requests, and routes each of these requests to an appropriate access point. The access points are defined in the README, and can also be accessed using curl commands from the terminal.
I’ve used Flask-SQLAlchemy to manage the SQLite database that I used for my implementation. SQLAlchemy maps relational databases to objects using predefined models.
Finally, I’ve displayed the output using AngularJS, which prints the database information in the form of a neat table. It uses a controller to store JSON object information, and then loops over it to display the output. The code is available here along with a README which has all the required information (installation instructions, API access points, etc).

##Resources
https://flask-stormpath.readthedocs.org/en/latest/product.html#enforce-user-authentication

https://www.ebi.ac.uk/ega/submission/tools/EGA_webin

https://www.ebi.ac.uk/ena/submit/preparing-xmls

https://www-test.ebi.ac.uk/ena/submit/drop-box/submit/

http://vyaskn.tripod.com/search_all_columns_in_all_tables.htm

http://stackoverflow.com/questions/16047306/how-is-docker-different-from-a-normal-virtual-machine

https://realpython.com/blog/python/dockerizing-flask-with-compose-and-machine-from-localhost-to-the-cloud/

https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-python-web-applications 

https://www.ebi.ac.uk/ega/submission/tools/FTP_aspera

https://docs.python.org/2/library/hashlib.html 





